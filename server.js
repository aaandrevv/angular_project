require("dotenv").config();
const express = require("express");
const mongoose = require("mongoose");
const morgan = require("morgan");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const cors = require("cors");
const {filePath} = require("./src/middlewares/filePathMiddleware");

const {authRouter} = require("./src/routers/authRouter");
const {boardsRouter} = require("./src/routers/boardsRouter");
const {usersRouter} = require("./src/routers/usersRouter");
const path = require("path");

const uri = `mongodb+srv://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@cluster0.ihru70j.mongodb.net/task-control?retryWrites=true&w=majority`;

const app = express();
app.use(cors({
    origin: ['https://task-control-app.vercel.app', 'https://localhost:8080'],
    credentials: true
}));
app.use(filePath(path.resolve(__dirname, 'public', 'images')));
app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(morgan('tiny'));
app.use(cookieParser());

app.use('/api/auth', authRouter);
app.use('/api/boards', boardsRouter);
app.use('/api/users', usersRouter);

(async () => {
    try {
        const port = process.env.PORT || 3000;
        await mongoose.connect(uri);
        app.listen(port, () => {
            console.log(`Server has been started on port ${port}`)
        });
    } catch (err) {
        console.log(`Error on server startup: ${err}`);
    }
})();

