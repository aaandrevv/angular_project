const mongoose = require("mongoose");
const {comment} = require("../models/Comment");

const task = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    status: {
        type: String,
        default: "TODO",
        enum: ["TODO", "IN PROGRESS", "DONE"],
        required: true
    },
    isArchived: {
        type: Boolean,
        default: false,
        required: true
    },
    image: {
        type: String
    },
    comments: [comment]
}, {
    timestamps: true
})

module.exports = {task};
