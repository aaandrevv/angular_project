const {task} = require("../models/Task");
const mongoose = require("mongoose");

const board = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    todoColor: {
        type: String
    },
    inProgressColor: {
        type: String
    },
    doneColor: {
        type: String
    },
    tasks: [task],
}, {
    timestamps: true
})

module.exports = mongoose.model('board', board);
