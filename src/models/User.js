const mongoose = require("mongoose");

const user = mongoose.Schema({
    username: {
        type: String,
        required: true,
        unique: true,
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    image: {
        type: String
    }
}, {
    timestamps: true
})

module.exports = mongoose.model("user", user);
