const mongoose = require("mongoose");

const comment = mongoose.Schema({
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user",
        required: true
    },
    comment: {
        type: String,
        required: true
    }
}, {
    timestamps: true
});

module.exports = {comment};
