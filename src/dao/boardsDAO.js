const Board = require("../models/Board");

const createBoardData = async (name, description, createdBy) => {
    const board = new Board({
        name,
        description,
        createdBy
    });
    await board.save();
    return board;
}

const getBoardsData = (createdBy) => {
    return Board.find({
        createdBy
    }).populate({
        path: 'tasks',
        populate: {
            path: 'comments',
            populate: {
                path: 'createdBy'
            }
        }
    });
}

const getBoardData = (_id, createdBy) => {
    return Board.findOne({
        _id,
        createdBy
    }).populate({
        path: 'tasks',
        populate: {
            path: 'comments',
            populate: {
                path: 'createdBy'
            }
        }
    });
}

const editBoardData = async (_id, createdBy, name, description, todoColor, inProgressColor, doneColor) => {
    await Board.findOneAndUpdate({
        _id,
        createdBy
    }, {
        name,
        description,
        todoColor,
        inProgressColor,
        doneColor
    });
}

const deleteBoardData = async (_id) => {
    await Board.findOneAndDelete({
        _id
    })
}

const createTaskData = async (_id, userId, name, description, status, isArchived, image) => {
    await Board.updateOne({
        _id,
        createdBy: userId
    }, {
        "$push": {
            tasks: {
                name,
                description,
                status,
                isArchived,
                image
            }
        }
    }, {
        runValidators: true
    })
}

const deleteTaskData = async (_id, userId, taskId) => {
    return Board.findOneAndUpdate({
        _id
    }, {
        $pull: {
            tasks: {
                _id: taskId
            }
        }
    });
}

const editTaskData = async (_id, createdBy, taskId, name, description, status, isArchived, image) => {
    await Board.updateOne({
        _id,
        createdBy,
        'tasks._id': taskId,
    }, {'$set': {
            'tasks.$.name': name,
            'tasks.$.description': description,
            'tasks.$.status': status,
            'tasks.$.isArchived': isArchived,
            'tasks.$.image': image
        }}, {
        runValidators: true
    });
}

const getTaskData = async (_id, taskId, createdBy) => {
    return Board.findOne({
        _id,
        createdBy
    }, {
        tasks: {
            "$elemMatch": {
                "_id": taskId
            }
        },
    })
}

const getTasksData = async (_id, createdBy) => {
    return Board.findOne({
        _id,
        createdBy
    }).select("tasks");
}

const addCommentData = async (_id, taskId, userId, comment) => {
    await Board.updateOne({
        _id,
        "createdBy": userId,
        "tasks._id": taskId
    }, {
        "$push": {
            "tasks.$.comments": {
                "createdBy": userId,
                comment
            }
        }
    })
}

const deleteCommentData = async (_id, taskId, userId, commentId) => {
    await Board.updateOne({
        _id,
        "createdBy": userId,
        "tasks._id": taskId
    }, {
        "$pull": {
            "tasks.$.comments": {
                "_id": commentId
            }
        }
    })
}

module.exports = {
    createBoardData,
    getBoardsData,
    getBoardData,
    editBoardData,
    deleteBoardData,
    createTaskData,
    deleteTaskData,
    editTaskData,
    getTaskData,
    getTasksData,
    addCommentData,
    deleteCommentData
}
