const User = require("../models/User");

const createUserData = async (username, email, password) => {
    const user = new User({
        username,
        email,
        password,
    });
    await user.save();
    return user;
}

const getUserDataByEmail = async (email) => {
    return User.findOne({
        email
    });
}

const getUserDataById = async (_id) => {
    return User.findOne({
        _id
    });
}

const editUserData = async (_id, username, email, password, image) => {
    await User.updateOne({
        _id
    }, {
        username,
        email,
        password,
        image
    })
}

module.exports = {
    createUserData,
    getUserDataByEmail,
    getUserDataById,
    editUserData
}
