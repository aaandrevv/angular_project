const fs = require("fs");
const path = require("path");

const {
    getUserDataByEmail,
    getUserDataById,
    editUserData
} = require("../dao/usersDAO");
const bcrypt = require("bcryptjs");

const getUser = async (req, res, next) => {
    try {
        const {email} = req.user;
        const user = await getUserDataByEmail(email);
        res.status(200).json({
            user: {
                username: user.username,
                email: user.email,
                image: user.image
            }
        })
    } catch (err) {
        res.status(400).json({
            "message": err.message
        })
    }
}

const editUser = async (req, res, next) => {
    try {
        const {userId} = req.user;
        const {username, email, password} = req.body;
        const image = req.file?.filename;
        if (image) {
            await deleteUserImage(req, userId);
        }
        await editUserData(userId, username, email, password ? await bcrypt.hash(password, 10) : undefined, image);
        const user = await getUserDataById(userId);
        res.status(200).json({ user });
    } catch (err) {
        res.status(400).json({
            "message": err.message
        })
    }
}

const deleteUserImage = async (req, userId) => {
    const {image} = await getUserDataById(userId);
    if (image && fs.existsSync(path.resolve(req.filePath, image))) {
        fs.unlinkSync(path.resolve(req.filePath, image));
    }
}

module.exports = {
    getUser,
    editUser
}
