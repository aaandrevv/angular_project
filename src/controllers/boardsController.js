const {
    createBoardData,
    getBoardsData,
    getBoardData,
    editBoardData,
    deleteBoardData,
    createTaskData,
    deleteTaskData,
    editTaskData,
    getTaskData,
    getTasksData,
    addCommentData,
    deleteCommentData
} = require("../dao/boardsDAO")
const fs = require("fs");
const path = require("path");
const {getUserDataById} = require("../dao/usersDAO");

const addBoard = async (req, res, next) => {
    try {
        const {name, description} = req.body;
        const {userId} = req.user;
        await createBoardData(name, description, userId);
        res.status(200).json({
            "boards": await getBoardsData(userId)
        });
    } catch (err) {
        res.status(400).json({
            "message": err.message
        });
    }
}

const getBoards = async (req, res, next) => {
    try {
        const {userId} = req.user;
        const boards = await getBoardsData(userId);
        res.status(200).json({
            boards
        })
    } catch (err) {
        res.status(400).json({
            "message": err.message
        });
    }
}

const getBoard = async (req, res, next) => {
    try {
        const {userId} = req.user;
        const {id} = req.params;
        const board = await getBoardData(id, userId);
        res.status(200).json({
            board
        })
    } catch (err) {
        res.status(400).json({
            "message": err.message
        });
    }
}

const editBoard = async (req, res, next) => {
    try {
        const {userId} = req.user;
        const {name, description, todoColor, inProgressColor, doneColor} = req.body;
        const {id} = req.params;
        await editBoardData(id, userId, name, description, todoColor, inProgressColor, doneColor);
        res.status(200).json({
            "boards": await getBoardsData(userId)
        })
    } catch (err) {
        res.status(400).json({
            "message": err.message
        });
    }
}

const deleteBoard = async (req, res, next) => {
    try {
        const {userId} = req.user;
        const {id} = req.params;
        await deleteBoardImages(req, id, userId);
        await deleteBoardData(id, userId);
        res.status(200).json({
            "boards": await getBoardsData(userId)
        })
    } catch (err) {
        res.status(400).json({
            "message": err.message
        });
    }
}

const deleteBoardImages = async (req, id, userId) => {
    const {tasks} = await getTasksData(id, userId);
    if (tasks.length === 0) {
        return;
    }
    for (let task of tasks) {
        if (task.image) {
            fs.unlinkSync(path.resolve(req.filePath, task.image));
        }
    }
}

const createTask = async (req, res, next) => {
    try {
        const {boardId} = req.params;
        const {userId} = req.user;
        const {name, description, status, isArchived, image} = req.body;
        await createTaskData(boardId, userId, name, description, status, isArchived, image);
        res.status(200).json({
            "board": await getBoardData(boardId, userId)
        });
    } catch (err) {
        res.status(400).json({
            "message": err.message
        });
    }
}

const getTask = async (req, res, next) => {
    try {
        const {boardId, taskId} = req.params;
        const {userId} = req.user;
        const findResult = await getTaskData(boardId, taskId, userId);
        res.status(200).json({
            task: findResult.tasks[0]
        });
    } catch (err) {
        res.status(400).json({
            "message": err.message
        });
    }
}

const getTasks = async (req, res, next) => {
    try {
        const {boardId} = req.params;
        const {userId} = req.user;
        const tasks = await getTasksData(boardId, userId);
        res.status(200).json({
            tasks
        });
    } catch (err) {
        res.status(400).json({
            "message": err.message
        });
    }
}

const deleteTask = async (req, res, next) => {
    try {
        const {boardId, taskId} = req.params;
        const {userId} = req.user;
        await deleteTaskImage(req, boardId, taskId, userId);
        await deleteTaskData(boardId, userId, taskId);
        res.status(200).json({
            "board": await getBoardData(boardId, userId)
        });
    } catch (err) {
        res.status(400).json({
            "message": err.message
        });
    }
}

const editTask = async (req, res, next) => {
    try {
        const {boardId, taskId} = req.params;
        const {userId} = req.user;
        const {name, description, status, isArchived} = req.body;
        const image = req.file?.filename;
        if (image) {
            await deleteTaskImage(req, boardId, taskId, userId);
        }
        await editTaskData(boardId, userId, taskId, name, description, status, isArchived, image);
        const {tasks} = await getTaskData(boardId, taskId, userId)
        res.status(200).json({
            "task": tasks[0],
            "boardId": boardId
        });
    } catch (err) {
        res.status(400).json({
            "message": err.message
        });
    }
}

const addComment = async (req, res, next) => {
    try {
        const {boardId, taskId} = req.params;
        const {userId} = req.user;
        const {comment} = req.body;
        await addCommentData(boardId, taskId, userId, comment);
        res.status(200).json({
            "comment": {
                createdBy: await getUserDataById(userId),
                comment,
                createdAt: new Date()
            },
            boardId,
            taskId,
        });
    } catch (err) {
        res.status(400).json({
            "message": err.message
        });
    }
}

const deleteComment = async (req, res, next) => {
    try {
        const {boardId, taskId, commentId} = req.params;
        const {userId} = req.user;
        await deleteCommentData(boardId, taskId, userId, commentId);
        res.status(200).json({
            "board": await getBoardData(boardId, userId)
        });
    } catch (err) {
        res.status(400).json({
            "message": err.message
        });
    }
}

const deleteTaskImage = async (req, id, taskId, createdBy) => {
    const {tasks} = await getTaskData(id, taskId, createdBy);
    const {image} = tasks[0];
    if (image && fs.existsSync(path.resolve(req.filePath, image))) {
        fs.unlinkSync(path.resolve(req.filePath, image));
    }
}

module.exports = {
    addBoard,
    getBoards,
    getBoard,
    editBoard,
    deleteBoard,
    createTask,
    deleteTask,
    editTask,
    getTask,
    getTasks,
    addComment,
    deleteComment
}
