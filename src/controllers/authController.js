const jwt = require("jsonwebtoken");
const bcrypt = require('bcryptjs');

const {
    createUserData,
    getUserDataByEmail, getUserDataById
} = require("../dao/usersDAO");

const tokenExpiration = 60 * 60 * 1000;

const signUp = async (req, res, next) => {
    try {
        const {username, email, password} = req.body;
        await checkUnique(email);
        const user = await createUserData(username, email, await bcrypt.hash(password, 10));
        setToken(res, user);
        res.status(200).json({
            id: user._id,
            username: user.username,
            email: user.email,
            image: user.image
        });
    } catch (err) {
        res.status(400).json({
            "message": err.message
        });
    }
}

const checkUnique = async (email) => {
    if (await getUserDataByEmail(email) !== null) {
        throw new Error("EMAIL_EXISTS");
    }
}

const logIn = async (req, res, next) => {
    try {
        const {email, password} = req.body;
        const user = await getUserDataByEmail(email);
        if (!user) {
            return res.status(401).json({
                "message": "USER_NOT_EXISTS"
            });
        }
        if (await bcrypt.compare(password, user.password)) {
            setToken(res, user);
            return res.status(200).json({
                id: user._id,
                username: user.username,
                email: user.email,
                image: user.image
            })
        } else {
            res.status(403).json({
                "message": "INVALID_PASSWORD"
            })
        }
    } catch (err) {
        res.status(400).json({
            "message": err.message
        });
    }
}

const setToken = (res, user) => {
    const token = jwt.sign({
        userId: user._id,
        username: user.username,
        email: user.email,
    }, process.env.SECRET_KEY);
    res.cookie('token', token, {
        httpOnly: true,
        sameSite: "none",
        secure: true,
        maxAge: tokenExpiration
    })
}

const logOut = async (req, res, next) => {
    try {
        res.clearCookie('token', {
            httpOnly: true,
            sameSite: "none",
            secure: true,
        });
        res.status(200).json({
            "message": "Success"
        });
    } catch (err) {
        res.status(400).json({
            "message": err.message
        });
    }
}

const isAuthenticated = async (req, res) => {
    try {
        const token = req.cookies['token'];
        if (!token) {
            return res.status(200).json({
                "status": "Not authenticated",
                "user": null
            });
        } else {
            const {userId} = jwt.verify(token, process.env.SECRET_KEY);
            const user = await getUserDataById(userId);
            return res.status(200).json({
                "status": "Authenticated",
                "user": {
                    "id": user._id,
                    "username": user.username,
                    "email": user.email,
                    "image": user.image
                }
            });
        }
    } catch (err) {
        res.status(400).json({
            "message": err.message
        });
    }
}

module.exports = {
    signUp,
    logIn,
    logOut,
    isAuthenticated
}
