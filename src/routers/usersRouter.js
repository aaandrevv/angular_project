const express = require("express");

const router = express.Router();

const {authMiddleware} = require("../middlewares/authMiddleware");

const {upload} = require("../middlewares/multerMiddleware");

const {
    getUser,
    editUser
} = require("../controllers/usersController");

router.get('/', authMiddleware, getUser);

router.put('/', authMiddleware, upload.single('image'), editUser);

module.exports = {
    usersRouter: router
}
