const express = require("express");

const router = express.Router();

const {
    authMiddleware
} = require("../middlewares/authMiddleware");

const {
    addBoard,
    getBoard,
    getBoards,
    editBoard,
    deleteBoard,
    createTask,
    deleteTask,
    editTask,
    getTask,
    getTasks,
    addComment,
    deleteComment
} = require("../controllers/boardsController");

const {upload} = require("../middlewares/multerMiddleware");

router.get('/', authMiddleware, getBoards);

router.post('/', authMiddleware, addBoard);

router.get('/:id', authMiddleware, getBoard);

router.patch('/:id', authMiddleware, editBoard);

router.delete('/:id', authMiddleware, deleteBoard);

router.get('/:boardId/tasks', authMiddleware, getTasks);

router.post('/:boardId/tasks', authMiddleware, createTask);

router.get('/:boardId/tasks/:taskId', authMiddleware, getTask);

router.delete('/:boardId/tasks/:taskId', authMiddleware, deleteTask);

router.patch('/:boardId/tasks/:taskId', authMiddleware, upload.single('image'), editTask);

router.patch('/:boardId/tasks/:taskId/comments', authMiddleware, addComment);

router.delete('/:boardId/tasks/:taskId/comments/:commentId', authMiddleware, deleteComment);

module.exports = {
    boardsRouter: router
}
