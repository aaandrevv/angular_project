const express = require("express");

const router = express.Router();

const {
    signUp,
    logIn,
    logOut,
    isAuthenticated
} = require('../controllers/authController');

const {authMiddleware} = require("../middlewares/authMiddleware");

router.post('/sign-up', signUp);

router.post('/login', logIn);

router.get('/logout', logOut);

router.get('/authenticated', isAuthenticated)

module.exports = {
    authRouter: router
};
