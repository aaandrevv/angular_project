const fs = require("fs");

function filePath(path) {
    return function (req, res, next) {
        if (!fs.existsSync(path)) {
            fs.mkdirSync(path);
        }
        req.filePath = path;
        next();
    }
}

module.exports = {
    filePath
};
