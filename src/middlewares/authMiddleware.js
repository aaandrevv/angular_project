const jwt = require("jsonwebtoken");

const authMiddleware = (req, res, next) => {
    try {
        const token = req.cookies['token'];
        if (!token) {
            return res.status(401).json({
                "message": "Not authenticated"
            });
        }
        req.user = jwt.verify(token, process.env.SECRET_KEY);
        next();
    } catch (err) {
        res.status(400).json({
            message: err.message
        });
    }
}

module.exports = {
    authMiddleware
}
